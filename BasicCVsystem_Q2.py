#python program for basic CV operations
#Ritika Agarwal

#Command Line Arguments for this python script
#path of video or folder
#r9 : Rotate each frame by 90 degrees
#gs : Convert the frame to grayscale
#nv : Save the output as a new video

#press 'q' to exit the program

try:
    #importing required modules
    import cv2
    import time
    import sys
    import glob


    # total arguments
    n = len(sys.argv)
    print("Total arguments passed:", n)
     
    # Arguments passed
    r9 = False
    gs = False
    nv = False
    path = "path not given"  #path variable
    print("\nName of Python script:", sys.argv[0])
    print("\nArguments passed:", end = " ")
    for i in range(1, n):
        print(sys.argv[i], end = " ")
        #settings variables to use CLA(command line argument)
        if sys.argv[i] == 'r9':
            r9 = True
        elif sys.argv[i] == 'gs':
            gs = True
        elif sys.argv[i] == 'nv':
            nv = True
        else:
            path = sys.argv[i]  #path of folder for example : 'E:\CariKture\power automate'
    print("\n") #for proper output text


    #all video formats
    v_formats = ['.flv', '.f4v', '.f4p', '.f4a', '.f4b','.nsv','.webm','.mkv','.flv','.vob','.ogv', '.ogg','.drc','.gif','.gifv','.mng','.avi','.mts','.m2ts','.ts','.mov','.qt','.wmv','.yuv','.roq','.mxf','.3g2','.3gp','.svi','.m4v','.mpg','.mpeg','.m2v','.mp2','.mpe','.mpv','.mp4','.m4p','.m4v','.amv','.asf','.viv','.rmvb','.rm']

    #taking LIST OF files from folder
    all_files = glob.glob(path+"\*")

    #filtering only video type files from folder
    v_files = []
    for i in all_files:
        for j in v_formats:
            if j in i:
                v_files.append(i)  #all video files path


    #doing operations on every file
    count = 0
    for i in v_files:
        # used to record the time when we processed last frame
        prev_frame_time = 0 
        # used to record the time at which we processed current frame
        new_frame_time = 0
        # This will return video from the first webcam on your computer.
        cap = cv2.VideoCapture(i)
        #total number of frames in video
        #frame_number = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        # Get width and height
        frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        #setting new video variable
        if r9 == True:
            frame_width,frame_height = frame_height,frame_width
        if nv == True:
            #new name for every new video
            o_filename = 'Output'+str(count)+'.avi'
            if gs == True:
                out = cv2.VideoWriter(o_filename, cv2.VideoWriter_fourcc('X', 'V', 'I', 'D'), 30.0, (frame_width, frame_height),0)
                """ VideoWriter is a function used to save video in opencv.Takes five parameters
                   :param filename : Name of the output video file.
                   :param fourcc : 4-character code of codec used to compress the frames.
                   :param fps: Framerate of the created video stream. 
                   :param frameSize : Size of the video frames.
                   :param isColor : If it is not zero, the encoder will expect and encode color frames, otherwise it will work with grayscale frames.
                   :return: It returns an output video."""
        
            else:
                out = cv2.VideoWriter(o_filename, cv2.VideoWriter_fourcc('X', 'V', 'I', 'D'), 30.0, (frame_width, frame_height))
                """ VideoWriter is a function used to save video in opencv.Takes four parameters
                   :param filename : Name of the output video file.
                   :param fourcc : 4-character code of codec used to compress the frames.
                   :param fps: Framerate of the created video stream. 
                   :param frameSize : Size of the video frames.
                   :return: It returns an output video. """
            count+=1 
        # loop runs if capturing has been initialized.
        while(True):
            ret, frame = cap.read()
            if ret == False:
                break

            new_frame = frame
            if gs == True and r9 == True:
                #frame is rotated in 90 degree clockwise
                new_frame = cv2.rotate(new_frame, cv2.ROTATE_90_CLOCKWISE)
                """ cv2.rotate() method is used to rotate a 2D array in multiples of 90 degrees.Takes two parameters
                  :param src: It is the image whose color space is to be changed.
                  :param fourcc : It is an enum to specify how to rotate the array.
                  :return: It returns an image. """
                # frame is converted to gray
                new_frame = cv2.cvtColor(new_frame, cv2.COLOR_BGR2GRAY)
                """cv2.cvtColor() method is used to convert an image from one color space to another.Takes two parameters
                 :param src: It is the image whose color space is to be changed.
                 :param code: It is the color space conversion code.
                 :return: It returns an image. """
            elif gs == False and r9 == True:
                #frame is rotated in 90 degree clockwise
                new_frame = cv2.rotate(new_frame, cv2.ROTATE_90_CLOCKWISE)
                """cv2.rotate() method is used to rotate a 2D array in multiples of 90 degrees.Takes two parameters
                  :param src: It is the image whose color space is to be changed.
                  :param fourcc : It is an enum to specify how to rotate the array.
                  :return: It returns an image. """
            elif gs == True and r9 == False:
                # frame is converted to gray
                new_frame = cv2.cvtColor(new_frame, cv2.COLOR_BGR2GRAY)
            """'cv2.cvtColor() method is used to convert an image from one color space to another.Takes two parameters.
                 :param src: It is the image whose color space is to be changed.
                 :param code: It is the color space conversion code.
                 :return: It returns an image. """
            if nv == True:
                # output the frame
                out.write(new_frame)

            # The original input frame is shown in the window
            #cv2.imshow('Original', frame)
            # time when we finish processing for this frame
            new_frame_time = time.time()
         
            # Calculating the fps
         
            # fps will be number of frame processed in given time frame
            # since their will be most of time error of 0.001 second
            # we will be subtracting it to get more accurate result
            fps = 1/(new_frame_time-prev_frame_time)
            prev_frame_time = new_frame_time
         
            # converting the fps into integer
            fps = int(fps)
         
            # converting the fps to string so that we can display it on frame
            # by using putText function
            fps = str(fps)

            # putting the FPS count on the frame
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(new_frame, fps, (10, 70), font, 2, (100, 0, 255), 3)
            """ cv2.putText() method is used to draw a text string on any image.Takes eight parameters
                :param image: It is the image on which text is to be drawn.
                :param text: Text string to be drawn.
                :param org: It is the coordinates of the bottom-left corner of the text string in the image. 
                :param font: It denotes the font type. Some of font types are FONT_HERSHEY_SIMPLEX, FONT_HERSHEY_PLAIN, , etc.
                :param fontScale: Font scale factor that is multiplied by the font-specific base size.
                :param color: It is the color of text string to be drawn. For BGR, we pass a tuple. eg: (255, 0, 0) for blue color.
                :param thickness: It is the thickness of the line in px.
                :param lineType: This is an optional parameter.It gives the type of the line to be used.
                :return: It returns an image. """
            # The window showing the operated video stream
            #cv2.imshow('original video', frame)
            new_resize= cv2.resize(new_frame, (640, 480))
            """ cv2.resize() function is that the tuple passed for determining the size of the new image
              follows the order (width, height) unlike as expected (height, width).Takes two parameters
              :param src: It is the image whose size is to be changed.
              :param tuple : It is the tuple in the format (height,width) for resizing.
              :return: It returns numpy array. """
            cv2.imshow('converted video', new_resize)
            """ cv2.imshow() method is used to display an image in a window. Takes two parameters
               :param window_name: A string representing the name of the window in which image to be displayed. 
               :param image: It is the image that is to be displayed.
               :return: It doesn’t returns anything. """
                    
            # Wait for 'a' key to stop the program
            if cv2.waitKey(5) & 0xFF == ord('q'):
                break
        print("Video Complete")

        # Close the window / Release webcam
        cap.release()

        # After we release our webcam, we also release the output
        out.release()

        # De-allocate any associated memory usage
        cv2.destroyAllWindows()


#to print error to screen
except OSError as error:
    print("Error : ",error)

except Exception as ex:
    print("Error : ",ex)
